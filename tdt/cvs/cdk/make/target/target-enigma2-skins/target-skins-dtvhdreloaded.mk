#
# AR-P buildsystem smart Makefile
#
package[[ target_enigma2_skins_dtvhdreloaded

BDEPENDS_${P} = $(target_filesystem)

PV_${P} = git
PR_${P} = 1
PACKAGE_ARCH_${P} = all

call[[ base ]]

rule[[
  git://github.com/Taapat/skin-dTV-HD-Reloaded
]]rule

call[[ git ]]

$(TARGET_${P}).do_prepare: $(DEPENDS_${P})
	$(PREPARE_${P})
	touch $@


$(TARGET_${P}).do_package: $(TARGET_${P}).do_prepare
	$(PKDIR_clean)
	cd $(DIR_${P}) && cp -dpR $(DIR_${P})/usr  $(PKDIR)
	touch $@

call[[ ipk ]]

NAME_${P} = enigma2-plugin-skin-dtv-hd-reloaded
DESCRIPTION_${P} = Skin dTV-HD-Reloaded for enigma2
FILES_${P} = /usr

call[[ ipkbox ]]

]]package
