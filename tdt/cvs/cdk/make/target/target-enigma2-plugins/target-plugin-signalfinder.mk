#
# AR-P buildsystem smart Makefile
#
package[[ target_plugin_signalfinder

PV_${P} = git
PR_${P} = 1
PACKAGE_ARCH_${P} = all

call[[ base ]]

rule[[
  git://github.com/Dima73/enigma2-plugin-signalfinder.git:b=taapat
]]rule

call[[ git ]]

$(TARGET_${P}).do_prepare: $(DEPENDS_${P})
	$(PREPARE_${P})
	touch $@

$(TARGET_${P}).do_compile: $(TARGET_${P}).do_prepare
	cd $(DIR_${P}) && \
	$(crossprefix)/bin/python ./setup.py build
	touch $@

$(TARGET_${P}).do_package: $(TARGET_${P}).do_compile
	$(PKDIR_clean)
	cd $(DIR_${P}) && \
	$(crossprefix)/bin/python ./setup.py install \
	--install-purelib=$(PKDIR)/usr/lib/enigma2/python/Plugins
	touch $@

NAME_${P} = enigma2-plugin-systemplugins-signalfinder
DESCRIPTION_${P} = Signal finder for DVB-S2 tuners
MAINTAINER_${P} = Dima73 dima-73@inbox.lv

FILES_${P} = /usr/lib/enigma2/python/Plugins/SystemPlugins/Signalfinder/*

call[[ ipkbox ]]

]]package
