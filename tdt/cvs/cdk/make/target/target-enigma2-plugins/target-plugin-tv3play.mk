#
# AR-P buildsystem smart Makefile
#
package[[ target_plugin_tv3play

PV_${P} = git
PR_${P} = 1
PACKAGE_ARCH_${P} = all

call[[ base ]]

rule[[
  git://github.com/Taapat/enigma2-plugin-tv3play.git:b=master
]]rule

call[[ git ]]

$(TARGET_${P}).do_prepare: $(DEPENDS_${P})
	$(PREPARE_${P})
	touch $@

$(TARGET_${P}).do_compile: $(TARGET_${P}).do_prepare
	cd $(DIR_${P}) && \
	$(crossprefix)/bin/python ./setup.py build
	touch $@

$(TARGET_${P}).do_package: $(TARGET_${P}).do_compile
	$(PKDIR_clean)
	cd $(DIR_${P}) && \
	$(crossprefix)/bin/python ./setup.py install \
	--install-purelib=$(PKDIR)/usr/lib/enigma2/python/Plugins
	touch $@

NAME_${P} = enigma2-plugin-extensions-tv3play
DESCRIPTION_${P} = Watch TV3 play online services
MAINTAINER_${P} = Taapat taapat@gmail.com

FILES_${P} = /usr/lib/enigma2/python/Plugins/Extensions/TV3Play/*

call[[ ipkbox ]]

]]package
