#
# AR-P buildsystem smart Makefile
#
package[[ target_distro_feed_configs

BDEPENDS_${P} =

PV_${P} = 0.1
PR_${P} = 2
SRC_URI_${P} = Taapat OpenAR-P
PACKAGE_ARCH_${P} = $(box_arch)

DESCRIPTION_${P} = Configuration files for online package repositories aka feeds

call[[ base ]]

rule[[
  pdircreate:${DIR}
  install:-d:$(PKDIR)/etc/opkg
]]rule

$(TARGET_${P}).do_prepare: $(DEPENDS_${P})
	$(PREPARE_${P})
	touch $@

$(TARGET_${P}).do_package: $(TARGET_${P}).do_prepare
	$(PKDIR_clean)
	cd $(DIR_${P}) && $(INSTALL_${P})
ifeq ($(strip $(CONFIG_MINIMAL)),y)
	echo "src/gz Taapat http://taapat.ho.ua/Taapat-minimal" > $(PKDIR)/etc/opkg/official-feed.conf
else
	echo "src/gz Taapat http://taapat.ho.ua/Taapat-full" > $(PKDIR)/etc/opkg/official-feed.conf
endif
	echo "src/gz system http://taapat.ho.ua/system" > $(PKDIR)/etc/opkg/system-feed.conf
	echo "src/gz plugins http://taapat.ho.ua/plugins" > $(PKDIR)/etc/opkg/plugins-feed.conf
	echo "src/gz skins http://taapat.ho.ua/skins" > $(PKDIR)/etc/opkg/skins-feed.conf

	touch $@

call[[ ipkbox ]]

]]package
